#![allow(non_snake_case)]			// turn off warning about snake_case name
#![allow(non_camel_case_types)]		// turn off warning about upper camel case type name
#![allow(dead_code)]		// turn off warning about upper camel case type name


pub mod Mips;


#[derive (Clone, Copy, Debug, Eq, PartialEq)]
pub enum Endian
{
	LITTLE,
	BIG,
}


#[cfg(test)]
mod tests {
	#[test]
	fn it_works() {
		let result = 2 + 2;
		assert_eq!(result, 4);
	}
}
