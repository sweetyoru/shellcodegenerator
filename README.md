# 1. What is this?

This is a crate for quickly generate shellcode to use in exploit development.


# 2. Why?

There is pwntools.shellcraft, but it's written in python, and not everyone like to write their exploit in python, so this crate aim to become the rust version of it.


# 3. Current state of development:

Only mips/linux is usable now, other platform/architecture is non-exsitence. So if you want support for those, bring your own code.


**Pull request and bug report are welcome.**
